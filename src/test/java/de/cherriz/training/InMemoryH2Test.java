package de.cherriz.training;

import de.cherriz.training.bpmn.party.service.weather.internal.WeatherServiceMock;
import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.consulting.process_test_coverage.ProcessTestCoverage;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.init;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.processEngine;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;
import static org.junit.Assert.assertEquals;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class InMemoryH2Test {

    @Rule
    public ProcessEngineRule rule = new ProcessEngineRule();

    private static final String PROCESS_DEFINITION_KEY = "cherriz-training-bpmn";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        init(rule.getProcessEngine());
        System.setProperty("spring.profiles.active", "test");
    }

    /**
     * Just tests if the process definition is deployable.
     */
    @Test
    @Deployment(resources = "process.bpmn")
    public void parsingAndDeployment() {
        // nothing is done here, as we just want to check for exceptions during deployment
    }

    @Test
    @Deployment(resources = "process.bpmn")
    public void happyPath() {
        // Startparameter aufbereiten
        Map<String, Object> variables = new HashMap<>();
        variables.put("title", "Testveranstaltung");
        variables.put("location", WeatherServiceMock.GOOD_WEAHTER_LOCATION);
        Calendar date = Calendar.getInstance(Locale.GERMANY);
        date.add(Calendar.DAY_OF_MONTH, 2);
        variables.put("date", date.getTime());
        variables.put("description", "Testbeschreibung");

        // Prozess starten
        ProcessInstance processInstance = processEngine().getRuntimeService().startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);

        // Prüfen ob der Prozess bei der Terminübersicht steht.
        assertThat(processInstance).task().hasDefinitionKey("showOverview");

        // Schritt bestätigen
        complete(task());

        // Prüfen ob Prozess jetzt abgeschlossen ist
        assertThat(processInstance).isEnded();
    }

    @Test
    @Deployment(resources = "process.bpmn")
    public void reschedule() {
        // Startparameter aufbereiten
        Map<String, Object> variables = new HashMap<>();
        variables.put("title", "Testveranstaltung");
        variables.put("location", WeatherServiceMock.BAD_WEAHTER_LOCATION);
        Calendar date = Calendar.getInstance(Locale.GERMANY);
        date.add(Calendar.DAY_OF_MONTH, 2);
        variables.put("date", date.getTime());
        variables.put("description", "Testbeschreibung");

        // Prozess starten
        ProcessInstance processInstance = processEngine().getRuntimeService().startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);

        // Prüfen ob der Prozess bei der Handlungsoptionen erfragen steht.
        assertThat(processInstance).task().hasDefinitionKey("askOptions");

        // Schritt bestätigen
        complete(task(), withVariables("changeDetails", Boolean.TRUE));

        // Prüfen ob der Prozess bei der Neu Planen steht.
        assertThat(processInstance).task().hasDefinitionKey("reschedule");

        // Ort ändern und Schritt bestätigen
        complete(task(), withVariables("location", WeatherServiceMock.GOOD_WEAHTER_LOCATION));

        // Prüfen ob der Prozess bei der Terminübersicht steht.
        assertThat(processInstance).task().hasDefinitionKey("showOverview");

        // Schritt bestätigen
        complete(task());

        // Prüfen ob Prozess jetzt abgeschlossen ist
        assertThat(processInstance).isEnded();
    }

    @Test
    @Deployment(resources = "process.bpmn")
    public void future() {
        // Startparameter aufbereiten
        Map<String, Object> variables = new HashMap<>();
        variables.put("title", "Testveranstaltung");
        variables.put("location", WeatherServiceMock.GOOD_WEAHTER_LOCATION);
        Calendar date = Calendar.getInstance(Locale.GERMANY);
        date.add(Calendar.DAY_OF_MONTH, 10);
        variables.put("date", date.getTime());
        variables.put("description", "Testbeschreibung");

        // Prozess starten
        ProcessInstance processInstance = processEngine().getRuntimeService().startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);

        // Prüfen ob der Prozess bei Meldung Prüfung später steht
        assertThat(processInstance).task().hasDefinitionKey("notificationLater");
        complete(task());

        //Prüfen ob Prozess am Timer wartet
        assertThat(processInstance).isWaitingAt("waitTimer");

        //Timer ablaufen lassen
        execute(job());

        // Prüfen ob der Prozess bei der Terminübersicht steht.
        assertThat(processInstance).task().hasDefinitionKey("showOverview");

        // Schritt bestätigen
        complete(task());

        // Prüfen ob Prozess jetzt abgeschlossen ist
        assertThat(processInstance).isEnded();
    }

    @After
    public void calculateCoverageForAllTests() throws Exception {
        ProcessTestCoverage.calculate(rule.getProcessEngine());
    }

}