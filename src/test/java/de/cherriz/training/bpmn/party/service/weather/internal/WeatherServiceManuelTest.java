package de.cherriz.training.bpmn.party.service.weather.internal;

import de.cherriz.training.bpmn.party.service.ServiceContext;
import de.cherriz.training.bpmn.party.service.weather.Weather;

import java.util.Calendar;

public class WeatherServiceManuelTest {

    public static void main(String args[]) {
        WeatherServiceImpl service = ServiceContext.getBean(WeatherServiceImpl.class);
        Weather weather = service.getWeather(Calendar.getInstance().getTime(), "Schwäbisch Hall");
        System.out.println(weather);
    }

}
