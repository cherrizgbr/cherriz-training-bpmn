package de.cherriz.training.bpmn.party.service.weather.internal;

import de.cherriz.training.bpmn.party.service.weather.Weather;
import de.cherriz.training.bpmn.party.service.weather.WeatherService;
import de.cherriz.training.bpmn.party.service.weather.WeatherType;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Profile("test")
public class WeatherServiceMock implements WeatherService {

    public static final String BAD_WEAHTER_LOCATION = "Bad Weather City";

    public static final String GOOD_WEAHTER_LOCATION = "Good Weather City";

    @Override
    public Weather getWeather(Date date, String location) {
        switch (location){
            case BAD_WEAHTER_LOCATION:
                return new Weather(80, 3, WeatherType.Regen);
            case GOOD_WEAHTER_LOCATION:
                return new Weather(3, 20, WeatherType.Sonne);
            default:
                return new Weather(10, 12, WeatherType.Wolken);
        }
    }

}
