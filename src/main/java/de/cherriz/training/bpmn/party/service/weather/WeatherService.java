package de.cherriz.training.bpmn.party.service.weather;

import java.util.Date;

public interface WeatherService {
	
	Weather getWeather(Date date, String location);

}
