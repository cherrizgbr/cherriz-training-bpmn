package de.cherriz.training.bpmn.party.service.weather;

import java.io.Serializable;

public class Weather implements Serializable{

	private int windSpeed;

	private int temperature;

	private WeatherType type;

	public Weather(int windSpeed, int temperature, WeatherType type) {
		this.windSpeed = windSpeed;
		this.temperature = temperature;
		this.type = type;
	}

	public int getWindSpeed() {
		return windSpeed;
	}

	public int getTemperature() {
		return temperature;
	}

	public WeatherType getType() {
		return type;
	}

	@Override
	public String toString() {
		return type + " " + temperature + "°C (Wind " + windSpeed + "m/s)";
	}

}
