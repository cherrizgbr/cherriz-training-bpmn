package de.cherriz.training.bpmn.party.delegate;

import de.cherriz.training.bpmn.party.service.weather.Weather;
import de.cherriz.training.bpmn.party.service.weather.WeatherType;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.value.ObjectValue;

public class RateWeatherDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        ObjectValue weatherWrapper = execution.getVariableTyped("weather");
        Weather weather = weatherWrapper.getValue(Weather.class);
        Boolean weatherOK = true;

        if (weather.getType() == WeatherType.Regen) {
            weatherOK = false;
        } else if (weather.getType() == WeatherType.Schnee) {
            weatherOK = false;
        } else if (weather.getWindSpeed() > 25) {
            weatherOK = false;
        } else if (weather.getTemperature() < 0) {
            weatherOK = false;
        } else if (weather.getTemperature() > 30) {
            weatherOK = false;
        }

        execution.setVariable("weatherOK", weatherOK);
    }

}
