package de.cherriz.training.bpmn.party.service.weather;

public enum WeatherType {
	
	Wolken("Wolken"),
	
	Sonne("Sonne"),
	
	Regen("Regen"),
	
	Schnee("Schnee");
	
	String desc;
	
	WeatherType(String desc) {
		this.desc = desc;
	}
	
	@Override
	public String toString(){
		return this.desc;
	}

}