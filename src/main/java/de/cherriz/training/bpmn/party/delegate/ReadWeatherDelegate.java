package de.cherriz.training.bpmn.party.delegate;

import de.cherriz.training.bpmn.party.service.ServiceContext;
import de.cherriz.training.bpmn.party.service.weather.Weather;
import de.cherriz.training.bpmn.party.service.weather.WeatherService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.value.DateValue;
import org.camunda.bpm.engine.variable.value.StringValue;

public class ReadWeatherDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        DateValue date = execution.getVariableTyped("date");
        StringValue location = execution.getVariableTyped("location");
        Weather weather = ServiceContext.getBean(WeatherService.class).getWeather(date.getValue(), location.getValue());
        execution.setVariable("weather", weather);
        execution.setVariable("weatherStr", weather.toString());
    }

}