package de.cherriz.training.bpmn.party.service;

import org.springframework.context.annotation.*;

@Configuration
@ComponentScan(basePackages = "de.cherriz.training.bpmn.party.service")
public class ServiceContext {

    private static AnnotationConfigApplicationContext classContext = null;

    public static <T> T getBean(Class<T> clazz) {
        if (ServiceContext.classContext == null) {
            classContext = new AnnotationConfigApplicationContext();
            classContext.getEnvironment().setDefaultProfiles("prod");
            classContext.register(ServiceContext.class);
            classContext.refresh();
        }
        return classContext.getBean(clazz);
    }

}