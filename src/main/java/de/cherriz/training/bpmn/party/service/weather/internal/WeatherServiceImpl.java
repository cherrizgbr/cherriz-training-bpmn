package de.cherriz.training.bpmn.party.service.weather.internal;


import de.cherriz.training.bpmn.party.service.weather.Weather;
import de.cherriz.training.bpmn.party.service.weather.WeatherService;
import de.cherriz.training.bpmn.party.service.weather.WeatherType;
import org.camunda.bpm.engine.impl.util.json.JSONArray;
import org.camunda.bpm.engine.impl.util.json.JSONObject;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
@Profile("prod")
public class WeatherServiceImpl implements WeatherService {

    private static Client client = null;

    private static Client getClient() {

        if (client == null) {
            client = ClientBuilder.newClient();
        }
        return client;
    }

    @Override
    public Weather getWeather(Date date, String location) {
        WebTarget target = getClient().target("http://api.openweathermap.org")
                .path("/data")
                .path("/2.5")
                .path("/forecast")
                .path("/daily")
                .queryParam("q", location)
                .queryParam("appid", "44db6a862fba0b067b1930da0d769e98")
                .queryParam("lang", "de")
                .queryParam("cnt", "7")
                .queryParam("units", "metric");
        String jsonStr = target.request(MediaType.APPLICATION_JSON_TYPE).get().readEntity(String.class);

        JSONObject json = new JSONObject(jsonStr);
        JSONArray list = json.getJSONArray("list");

        for (int i = 0; i < list.length(); i++) {
            JSONObject dayWeather = list.getJSONObject(i);
            if (convertToLocalDate(dayWeather.getLong("dt") * 1000).isEqual(convertToLocalDate(date.getTime()))) {
                Double temp = dayWeather.getJSONObject("temp").getDouble("eve");
                Double speed = dayWeather.getDouble("speed");
                Integer weatherID = dayWeather.getJSONArray("weather").getJSONObject(0).getInt("id");
                WeatherType weather = WeatherType.Regen;
                if (weatherID >= 600 && weatherID < 700) {
                    weather = WeatherType.Schnee;
                } else if (weatherID >= 700 && weatherID < 800) {
                    weather = WeatherType.Wolken;
                } else if (weatherID == 800 && weatherID < 900) {
                    weather = WeatherType.Sonne;
                } else if (weatherID > 800 && weatherID < 900) {
                    weather = WeatherType.Wolken;
                }
                return new Weather(speed.intValue(), temp.intValue(), weather);
            }
        }
        return null;
    }

    private LocalDate convertToLocalDate(Long millis) {
        Instant instant = Instant.ofEpochMilli(millis);
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
    }

}