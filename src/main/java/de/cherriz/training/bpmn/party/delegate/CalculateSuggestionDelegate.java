package de.cherriz.training.bpmn.party.delegate;

import de.cherriz.training.bpmn.party.service.weather.Weather;
import de.cherriz.training.bpmn.party.service.weather.WeatherType;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.value.ObjectValue;

public class CalculateSuggestionDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        ObjectValue weatherWrapper = execution.getVariableTyped("weather");
        Weather weather = weatherWrapper.getValue(Weather.class);

        String suggesstion = "Kleiden Sie sich ";
        if (weather.getTemperature() <= 10) {
            suggesstion += "warm";
        } else if (weather.getTemperature() < 20) {
            suggesstion += "normal";
        } else {
            suggesstion += "luftig";
        }

        if (weather.getType() == WeatherType.Sonne) {
            suggesstion += " sowie eine Sonnenbrille";
        }
        if (weather.getWindSpeed() > 15) {
            suggesstion += " und keinen Schirm, es wird Windig";
        }

        execution.setVariable("suggesstion", suggesstion);
    }

}
