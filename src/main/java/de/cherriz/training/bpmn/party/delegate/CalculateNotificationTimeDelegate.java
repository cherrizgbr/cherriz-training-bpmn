package de.cherriz.training.bpmn.party.delegate;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.value.DateValue;

public class CalculateNotificationTimeDelegate implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		DateValue date = execution.getVariableTyped("date");
		Calendar cal = Calendar.getInstance(Locale.GERMANY);
		cal.setTime(date.getValue());
		cal.add(Calendar.DAY_OF_MONTH, -3);
		execution.setVariable("notificationDate", cal.getTime());
	}
	
}
